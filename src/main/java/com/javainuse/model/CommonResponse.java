package com.javainuse.model;

import lombok.Data;

@Data
public class CommonResponse<T> {

    private int code;
    private String message;
    private T body;

}
