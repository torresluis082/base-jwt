package com.javainuse.model;

import lombok.Data;

@Data
public class ProductDTO {

    private long code;
    private String name;
    private String tipe;
    private String quantity;


}
