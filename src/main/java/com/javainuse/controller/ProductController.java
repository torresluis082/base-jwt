package com.javainuse.controller;

import com.javainuse.bdinmemory.ProductoInMemory;
import com.javainuse.model.CommonResponse;
import com.javainuse.model.ProductDTO;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin()
public class ProductController {

    @RequestMapping({"/registro"})
    public CommonResponse<ProductDTO> registro(@RequestBody ProductDTO dto) {
        CommonResponse<ProductDTO> response = new CommonResponse<>();

        ProductoInMemory.addToList(dto);
        return response;
    }

    @RequestMapping({"/buscar/{name}"})
    public CommonResponse<List<ProductDTO>> buscar(@PathVariable("name") String nombre) {
        CommonResponse<List<ProductDTO>> response = new CommonResponse<>();
        response.setBody(ProductoInMemory.getProductosListByName(nombre));
        return response;
    }

    @RequestMapping({"/listar"})
    public CommonResponse<List<ProductDTO>> listar() {
        CommonResponse<List<ProductDTO>> response = new CommonResponse<>();
        response.setBody(ProductoInMemory.getProductosList());
        return response;
    }

    @RequestMapping({"/eliminar/{id}"})
    public CommonResponse<String> buscar(@PathVariable("id") long code) {
        CommonResponse<String> response = new CommonResponse<>();
        boolean resultado = ProductoInMemory.removeById(code);

        if(resultado){
            response.setBody("Se elimino el registro");
        }else{
            response.setBody("No se pudo eliminar el registro");
        }
        return response;
    }

}
