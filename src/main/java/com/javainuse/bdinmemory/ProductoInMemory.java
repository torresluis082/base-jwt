package com.javainuse.bdinmemory;

import com.javainuse.model.ProductDTO;

import java.util.ArrayList;
import java.util.List;

public class ProductoInMemory {

    public static List<ProductDTO> productosList = new ArrayList<>();

    public static List<ProductDTO> getProductosList() {
        return productosList;
    }

    public static boolean addToList(ProductDTO dto) {
        boolean status = false;
        try {
            ProductoInMemory.productosList.add(dto);
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return status;
        }
    }

    public static List<ProductDTO> getProductosListByName(String nombre) {
        List<ProductDTO> local = new ArrayList<>();
        for (ProductDTO pd : getProductosList()) {
            if (pd.getName().toLowerCase().contains(nombre.toLowerCase())) {
                local.add(pd);
            }
        }
        return local;
    }

    public static void setProductosList(List<ProductDTO> productosList) {
        ProductoInMemory.productosList = productosList;
    }

    public static boolean removeById(Long code) {
        boolean status = false;
        try {
            for (ProductDTO pd : getProductosList()) {
                if (pd.getCode() == code) {
                    ProductoInMemory.productosList.remove(pd);
                    setProductosList(ProductoInMemory.productosList);
                }
            }
            status = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return status;
        }
    }

}
